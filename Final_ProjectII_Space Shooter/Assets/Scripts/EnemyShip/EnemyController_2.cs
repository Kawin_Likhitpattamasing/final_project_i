﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController_2 : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship_2 enemySpaceship_2;
        [SerializeField] private float chasingThresholdDistance;
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship_2.Fire();
        }

         private void MoveToPlayer()
         {
             
         }
    }    
}


using System;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip playerDestructionSound;
        [SerializeField] private float playerDestructionSoundVolume = 0.3f;
 
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(playerFireSound != null, "playerFireSound cannot be null");
            Debug.Assert(playerDestructionSound != null, "playerDestructionSound cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound,Camera.main.transform.position,playerFireSoundVolume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioSource.PlayClipAtPoint(playerDestructionSound,Camera.main.transform.position,playerDestructionSoundVolume);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}